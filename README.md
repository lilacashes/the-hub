# The Hub

A web app based on Vue.js for the client/frontend and Python Flask for the serve/backend.

## Requirements

You can also follow this instruction to run [Python Flask with Vue.js](https://testdriven.io/blog/developing-a-single-page-app-with-flask-and-vuejs/)

### Python

Make sure you have Python installed and run the newest version. For installation see [installation guide on realpython](https://realpython.com/installing-python/#macos-how-to-check-or-get-python). To check if you have Python installed open a terminal and type
```bash
$ python3 --version
```

### Vue.js

Make sure you have Vue.js installed and running. Vue.js is a frontend framework based on node.js. So first install [node.js](https://nodejs.org) then [Vue.js](https://vuejs.org). To check if you have Python installed open a terminal and type
```bash
$ node --version
$ 
```


## Installation and start

### Server

Install and activate Python Virtualenv. Go to directory the-hub and run (you can choose a different name for your Virtualenv - make sure to change it in .gitignore line 1):
```bash
$ python3 -m venv the-hub
$ . the-hub/bin/activate
```

To stop the Python Virtualenv run:
```bash
$ deactivate
```

Find more on Virtualenv [python.land on virtualenv](https://python.land/virtual-environments/virtualenv)

Install dependencies:
```bash
$ pip install -r requirements.txt
```

Check Flask version and update:
```bash
$ pip show flask
$ pip install --upgrade Flask
```

Run Flask app:
```bash
$ python3 server/app.py
```
The app runs now on [http://127.0.0.1:5000/ping](http://127.0.0.1:5000/ping).

### Client

Install the required node modules as specified in package.json:
```bash
$ cd client
$ npm install
```

Run Vue.js:
```bash
$ npm run dev
```
On [http://localhost:5173/ping](http://localhost:5173/ping) you will now get the data from Flask.
